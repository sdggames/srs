class_name InputRecorder extends Node

signal recorder_state_changed(state: RecorderState)

enum RecorderState {
	DISABLED = 0,
	IDLE = 1,
	RECORDING = 2,
	PLAYING = 3,
	CONTINUAL_PLAYBACK = 4,
}

## Instance this to synchronize game objects.
const PUPPETEER = preload("res://Game/DebuggingTools/InputRecorder/puppeteer.tscn")
const file_base_path = "user://input_recorder/"

@export var state := RecorderState.DISABLED:
	set(value):
		Global.replay_mode = value
		recorder_state_changed.emit(value)
	get:
		return Global.replay_mode

@export var event_file_name := "gameplay_01":
	set(value):
		event_file_name = value
		_path = file_base_path + event_file_name + ".dat"

## Version number for the input recorder itself. Bump this whenever there are breaking changes to ensure
## that we don't try to load an old input file.
@export var input_recorder_version := 0

var buttons = [] # List to store button nodes and their paths.
var physics_frames = []
# Things to track frame to frame.
var button_presses = []
var asteroids_spawned = []
var enemies_spawned = []
var objects := {}
var _path: String
var _log: Logger
var _active := false
var _exit_on_complete := true
var _repeat := false

@onready var asteroids = $"../../World/Asteroids"
@onready var enemies = $"../../World/Enemies"


func _ready():
	if state == RecorderState.DISABLED:
		queue_free() # Do this first, no need to waste time setting up.
	
	_log = Print.get_logger("InputRecorder")
	_path = file_base_path + event_file_name + ".dat"
	find_and_connect_buttons()
	
	## Make sure that the player exists and is connected.
	#await get_tree().process_frame
	#var player = Global.player
	#var puppeteer = PUPPETEER.instantiate()
	#puppeteer.properties.append_array([&"position", &"rotation", &"thrusting"])
	#player.add_child(puppeteer)
	#objects[&"player"] = puppeteer
	#_active = true
	
	# Start the recurding or playback if we set this in the editor before playing.
	if state == RecorderState.RECORDING:
		start_recording()
	elif state == RecorderState.PLAYING:
		start_replay()
	elif state == RecorderState.CONTINUAL_PLAYBACK:
		_repeat = true
		start_replay()


func _physics_process(_delta):
	if !_active:
		return
	if state == RecorderState.RECORDING:
		physics_frames.append(FrameCapture.capture_frame(button_presses.duplicate(), \
				asteroids_spawned.duplicate(), enemies_spawned.duplicate(), \
				{&"player": objects[&"player"].get_frame()}))
		button_presses.clear()
		asteroids_spawned.clear()
		enemies_spawned.clear()
	elif state == RecorderState.PLAYING:
		var frame_capture: FrameCapture = physics_frames.pop_front()
		# Button presses and player object.
		simulate_button_presses(frame_capture.button_presses)
		objects[&"player"].set_frame(frame_capture.puppets[&"player"])
		
		# Spawn objects.
		for a in frame_capture.asteroids_spawned:
			_log.verbose("Spawning loopback asteroid: %s, %s, %s" % a)
			asteroids._on_spawn_object(a[0], a[1], a[2])
		
		for e in frame_capture.enemies_spawned:
			_log.verbose("Spawning loopback enemy: %s, %s, %s" % e)
			enemies._on_spawn_object(e[0], e[1], e[2])
		
		# The game recording has ended. Quit.
		if physics_frames.is_empty():
			if _repeat:
				start_replay()
			elif _exit_on_complete:
				get_tree().quit()
			else:
				state = RecorderState.IDLE
				_log.info("Final frame reached, replay has stopped.")


## Start recording inputs to file.
func start_recording():
	if Global.game_state != Global.GameState.BOOT and Global.game_state != Global.GameState.MAIN_MENU:
		_log.error("Gameplay recording must start from the main menu ONLY!", false)
		return
	
	state = RecorderState.RECORDING
	_log.info("Recording game events to file %s." % event_file_name)


## Start playing inputs from a file.
func start_replay():
	if Global.game_state != Global.GameState.BOOT and Global.game_state != Global.GameState.MAIN_MENU:
		_log.error("Gameplay playback must start from the main menu ONLY!", false)
		return
	
	if load_inputs_from_file():
		_log.info("Replaying game events from file %s." % event_file_name)
		# Make sure to clear the objects array so we don't call the wrong object.
		objects = {&"player": objects[&"player"]}
		state = RecorderState.PLAYING
	else:
		state = RecorderState.IDLE


## Recursively scan the entire game for buttons, and connect them all to me.
func find_and_connect_buttons(current = null):
	if current == null:
		current = get_tree().root
	for button in current.get_children():
		find_and_connect_buttons(button)
		if button is Button:
			buttons.append(button)
			button.pressed.connect(_on_button_pressed.bind(button.get_path()))
			_log.verbose("Connected button at %s to Input Recorder." % button.get_path())


func _on_button_pressed(button_path):
	if state == RecorderState.RECORDING:
		button_presses.append(button_path)
		_log.verbose("Button press for %s recorded." % button_path)


func simulate_button_presses(presses):
	for press in presses:
		var button = get_node(press)
		if button:
			button.emit_signal("pressed")
			_log.verbose("Button press simulated for %s." % press)


## Save the recorded inputs to a file.
func save_inputs_to_file():
	if !DirAccess.dir_exists_absolute(file_base_path):
		DirAccess.make_dir_recursive_absolute(file_base_path)
	var file = FileAccess.open(_path, FileAccess.WRITE)
	if file:
		file.store_var(input_recorder_version)
		file.store_var(ProjectSettings.get_setting("application/config/version"))
		file.store_var(physics_frames)
		file.close()
	else:
		_log.error("Failed to save gameplay recording.")


## Loads a set of inputs from the file. If the file is invalid, throws an error, then returns false.
func load_inputs_from_file() -> bool:
	var file = FileAccess.open(_path, FileAccess.READ)
	if file:
		var recorder_version = file.get_var()
		if recorder_version < input_recorder_version:
			_log.error("Recording file is from a deprecated version of the Input Recorder. Aborting Replay." +\
					"(Our version: %s, file version %s)" % [input_recorder_version, recorder_version], false)
			return false
		var app_version = file.get_var()
		if app_version != ProjectSettings.get_setting("application/config/version"):
			_log.error("Recording file is from another version of the app! This might not work. Our version: %s, file version %s" % \
					[ProjectSettings.get_setting("application/config/version"), app_version], false)
		
		var raw_frames: Array = file.get_var()
		file.close()
		physics_frames.clear()
		for frame in raw_frames:
			physics_frames.append(FrameCapture.deserialize(frame))
		return true
	else:
		_log.error("Failed to load recording. Are you sure that %s is a valid file?" % _path, false)
		return false


func _exit_tree():
	if state == RecorderState.RECORDING:
		save_inputs_to_file()


func _on_start_playback():
	if state == RecorderState.IDLE:
		# Turn off because we are running from the menus instead of from boot.
		_repeat = false
		_exit_on_complete = false
		start_replay()
	else:
		_log.error("Attempted to start replay playback, but recorder state is %s state." % RecorderState.keys()[state], false)


func _on_start_recording():
	if state == RecorderState.IDLE:
		start_recording()
	else:
		_log.error("Attempted to start replay recording, but recorder state is %s state." % RecorderState.keys()[state], false)


func _on_debug_overlay_cancel_recording():
	if state == RecorderState.RECORDING:
		save_inputs_to_file()
	state = RecorderState.IDLE


func _on_debug_overlay_recorder_file_changed(text):
	event_file_name = text


## Loopback call from the asteroid spawner. 
func _on_asteroids_spawn_object(index, uid, params):
	_log.verbose("Loopback from asteroid: %s, %s, %s" % [index, uid, params])
	asteroids_spawned.append([index, uid, params])


func _on_enemies_spawn_object(index, uid, params):
	_log.verbose("Loopback from enemies: %s, %s, %s" % [index, uid, params])
	enemies_spawned.append([index, uid, params])


func _on_asteroids_object_spawned(_object, _id):
	pass # Replace with function body.
	# TODO: Connect a Puppeteer to track object stats (if needed?)


func _on_enemies_object_spawned(_object, _id):
	pass # Replace with function body.
	# TODO: Connect a Puppeteer to track object stats (if needed?)
