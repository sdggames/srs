class_name FrameCapture extends Resource
## Records major frame events and the position of all objects within a frame.
##
## Objects and puppeteers have shared IDs, so this class mostly just holds onto black box data
## and passes it on when requested. 99% of the work here is just saving to disk and retrieving.

## All button presses within the frame.
var button_presses: Array = []

## All asteroid spawn calls. This is a loopback, just pass it back to the spawn system.
var asteroids_spawned: Array = []

## All enemy spawn calls. This is a loopback, just pass it back to the spawn system.
var enemies_spawned: Array = []

## All puppeteer data. This is already formatted for serialization.
var puppets: Dictionary = {}


## Return the frame as an array that can be saved to disk.
func serialize() -> Array:
	return [button_presses, asteroids_spawned, enemies_spawned, puppets]


## Load a frame object from a previously serialized array.
static func deserialize(stream: Array) -> FrameCapture:
	if stream.size() != 4:
		Print.get_logger("InputRecorder").error("Frame Capture data is invalid! Playback will fail!", false)
		return empty()
	return new_frame(stream[0], stream[1], stream[2], stream[3])


## Returns an empty frame. Useful for padding the beginning of the array so we don't start playing back when things are loading.
static func empty() -> FrameCapture:
	return FrameCapture.new()


## Creates a frame from the given data.
## Returns the created frame.
static func new_frame(presses: Array, asteroids: Array, enemies: Array, ppts: Dictionary) -> FrameCapture:
	var capture = FrameCapture.new()
	capture.button_presses = presses
	capture.asteroids_spawned = asteroids
	capture.enemies_spawned = enemies
	capture.puppets = ppts
	return capture


## Creates a new frame, and then serilizes it. Equivalent to new_frame().serialize()
## This returns a saveable array, NOT a FrameCapture!
static func capture_frame(presses: Array, asteroids: Array, enemies: Array, ppts: Dictionary) -> Array:
	return new_frame(presses, asteroids, enemies, ppts).serialize()
