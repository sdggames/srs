class_name Puppeteer extends Node
## Records and replays properties and signals/inputs on the parent class.
##
## Can be extended for more complex synchronization.

# public variables
var properties: Array[StringName] = []

@onready var node = $".."


## Get a dictionary contining the properties for this frame.
func get_frame():
	var p = {}
	for property: StringName in properties:
		p[property] = node.get(property)
	return p


## Sets all of the properties for this frame from a dictionary.
func set_frame(puppet: Dictionary):
	for p in puppet.keys():
		node.set(p, puppet[p])
