extends Label


func _ready():
	_load_data()


func _load_data():
	visible = Global.load_other(Global.SETTINGS, "FPS Counter") == true


func _process(_delta):
	text = "FPS: " + str(Engine.get_frames_per_second())
