class_name AutoloadDebug extends Node
## Collection for all debugging things that don't go anywhere else.
##
## This is registered as the Debug global singleton.

# Loaded on game start.
var fps_visible := false


## Register all of the various debug options with the Console.
func _ready():
	Global.register_command("disable_splash", self, "_disable_splash", \
			"Disables the splash screen, jumping directly into the main scene instead.")
	Global.register_command("enable_splash", self, "_enable_splash", \
			"Enables the splash screen if it was disabled previously.")
	Global.register_command("open_user_folder", self, "_open_user_folder", \
			"Opens user:// in the file system..")
	Global.register_command("open_resource_folder", self, "_open_resource_folder", \
			"Opens res:// in the file system.")
	Global.register_command("toggle_fps", self, "_togggle_fps", "Turns the FPS counter on or off.")
	
	_load_data()


func _load_data():
	# Load from Global, could be null.
	fps_visible = (Global.load_other(Global.SETTINGS, "FPS Counter") == true)


## Turns off the splash screen animation. Useful when developing so I don't have to see it a bajillion times.
func _disable_splash():
	Global.save_data("Splash_Enabled", false)


## Turns the splash screen back on if it was previously disabled.
func _enable_splash():
	Global.save_data("Splash_Enabled", true)


## Turns the FPS counter on or off. This is also accesible via the Settings menu.
func _togggle_fps():
	fps_visible = !fps_visible
	Global.save_other(Global.SETTINGS, "FPS Counter" ,fps_visible)
	get_tree().call_group("SaveLoad", "_load_data")


## Opens the user folder in the OS.
func _open_user_folder():
	OS.shell_show_in_file_manager(OS.get_user_data_dir())


## Opens the resource folder in the OS.
func _open_resource_folder():
	OS.shell_show_in_file_manager(ProjectSettings.globalize_path("res://"))
