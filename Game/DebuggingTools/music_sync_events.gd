extends Node

@export var whole_count := 0
@export var is_whole := false
@export var tick1 := false
@export var tick2 := false
@export var tick3 := false
@export var tick4 := false

@export var quarter := 3
@export var eighth := 7
@export var sixteenth := 15

@export var print_to_console := false

## Copy this to get all of the group events.
@export_multiline var example_script := '''
## Called when a single note of the sound track is called.
## The note increments until the song (or page) ends, then they reset to zero.
## You can use (note % 4) to check if the note is starting a column thingy. 
func _music_tick(note: int):
	pass


## Called once per sixteenth note. Use the parameters to determine which partial note this is.
func _music_partial_tick(whole: bool, quarter: bool, eigth: bool):
	pass
'''

var _log: Logger


func _ready():
	_log = Print.get_logger("GroupEvents")
	# Don't waste cycles on a release build.
	if !OS.is_debug_build():
		queue_free()


## Called when a single note of the sound track is called.
func _music_tick(note: int):
	is_whole = true
	whole_count = note
	# Reset these after testing. We might occasionally have a single spare sixteenth
	# when the animation resets, but we should never have more than one full.
	_log.assert_that(quarter <= 4, "The sound track should have 4 quarters per note, but %s were detected!" % quarter)
	_log.assert_that(eighth <= 8, "The sound track should have 8 eighths per note, but %s were detected!" % eighth)
	_log.assert_that(sixteenth <= 16, "The sound track should have 16 sixteenths per note, but %s were detected!" % sixteenth)
	
	quarter = 0
	eighth = 0
	sixteenth = 0


## Called once per sixteenth note. Use the parameters to determine which partial note this is.
func _music_partial_tick(on_whole: bool, on_quarter: bool, on_eighth: bool):
	sixteenth += 1
	if on_eighth:
		eighth += 1
	if on_quarter:
		quarter += 1
		match quarter:
			1:
				is_whole = false
				tick1 = true
				tick2 = false
				tick3 = false
				tick4 = false
			2:
				tick1 = false
				tick2 = true
				tick3 = false
				tick4 = false
			3:
				tick1 = false
				tick2 = false
				tick3 = true
				tick4 = false
			4:
				tick1 = false
				tick2 = false
				tick3 = false
				tick4 = true
			_:
				_log.error("Extra quarter note detected! %s" % quarter)
	
	if print_to_console:
		print("Note count: Sixteenth %2d, Eigth %d, Quarter %d" % [sixteenth, eighth, quarter] +\
			" Whole: %s, Quarter: %s, Eighth: %s" % [on_whole, on_quarter, on_eighth])
