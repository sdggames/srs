extends Node

signal group_method_called(method_name: String)

## Copy this to get all of the group events.
@export_multiline var example_script := '''
## Called when the game data has been altered.
func _load_data():
	pass
'''

var _log: Logger


func _ready():
	_log = Print.get_logger("GroupEvents")


func _method_called(method_name: String):
	_log.info(method_name + " method called on SaveLoad group.")
	group_method_called.emit(method_name)


#This group listens for signals related to saving and loading.
## Called when the game data has been altered.
func _load_data():
	_method_called("_load_data")
