extends Node

signal group_method_called(method_name: String)

## Copy this to get all of the group events.
@export_multiline var example_script := '''
## Called before the level fade-in transition.
func _game_start():
	pass


## Called when the level starts or the player respawns.
func _player_spawned():
	pass


## Called every time the player dies.
func _player_killed():
	pass


## Called immediately when the player dies. Play elements should remain active and visible.
func _game_over():
	pass


## Called when the game has successfully transitioned to the game menu. All play elements are hidden and can be destroyed.
func _game_reset():
	pass
'''

var _log: Logger


func _ready():
	_log = Print.get_logger("GroupEvents")


func _method_called(method_name: String):
	_log.info(method_name + " method called on GameEvents group.")
	group_method_called.emit(method_name)


#This group listens for signals related to stopping and starting.
## Called before the level fade-in transition.
func _game_start():
	_method_called("_game_start")


## Called when the level starts or the player respawns.
func _player_spawned():
	_method_called("_player_spawned")


## Called every time the player dies.
func _player_killed():
	_method_called("_player_killed")


## Called immediately when the player dies. Play elements should remain active and visible.
func _game_over():
	_method_called("_game_over")


## Called when the game has successfully transitioned to the game menu. All play elements are hidden and can be destroyed.
func _game_reset():
	_method_called("_game_reset")
