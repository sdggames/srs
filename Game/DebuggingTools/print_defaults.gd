## Stores the print defaults in an easily accessible node in the main game scene. Sends the names
## to the Global singleton to preload the loggers, but overwrites their values later when this
## class is added to the scene tree. After doing its job, the class is removed so I don't try to
## change the values here while the game is running (which does nothing, [Print] is maintaining the
## loggers at that point.)
class_name PrintDefaults extends Node

## Defines all of the loggers in the project. All loggers should be defined in this const so there
## are no initialization errors. We can safely print from [method _ready] functions this way.
## All loggers will be set to [constant default_console_level] [constant default_archive_level]
## by the Global singleton, but will be reset to their proper values when this node is added
## to the scene.
const print_loggers = [
	# General
	"Global", # Any Print.level() calls
	# Dev Tools
	"Debugging", # Debugging tools or scripts that don't fit elsewhere.
	"GameState", # Global monitoring of the game state. Reports state changes.
	"GroupEvents", # Global monitoring of all group event calls.
	"InputRecorder", # Records and replays input events.
	# Game Systems
	"Game",
	"Progression",
	"Spawn",
	"Minerals",
	"Particles",
]

## The default logging level for all loggers in the [constant print_loggers] array. Is overridden when
## the [PrintDefaults] object loads in.
const default_console_level := Logger.LogLevel.INFO

## The default archive level for all loggers in the [constant print_loggers] array. Is overridden when
## the [PrintDefaults] object loads in.
const default_archive_level := Logger.LogLevel.VERBOSE

@export_group("General Logging")

@export_subgroup("Global Logger", "global_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var global_console_level := 5
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var global_archive_level := 5

@export_subgroup("Print Logger", "print_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var print_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var print_archive_level := 5


@export_group("Debug Logging")

@export_subgroup("Debugging Tools", "debug_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var debug_console_level := 3
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var debug_archive_level := 5

@export_subgroup("Game State Logger", "state_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var state_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var state_archive_level := 5

@export_subgroup("Group Events Logger", "events_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var events_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var events_archive_level := 5

@export_subgroup("Input Recorder", "input_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var input_console_level := 3
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var input_archive_level := 4


@export_group("Game Systems Logging")

@export_subgroup("Game Logger", "game_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var game_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var game_archive_level := 5

@export_subgroup("Player Level Progression", "prgss_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var prgss_console_level := 3
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var prgss_archive_level := 5

@export_subgroup("Spawn System Managers", "spawn_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var spawn_console_level := 3
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var spawn_archive_level := 4

@export_subgroup("Minerals", "mineral_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var mineral_console_level := 3
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var mineral_archive_level := 4

@export_subgroup("Particles", "particle_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var particle_console_level := 3
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var particle_archive_level := 4


func _ready():
	Print.create_logger("Global", global_console_level, global_archive_level)
	Print.create_logger("Debugging", debug_console_level, debug_archive_level)
	Print.create_logger("GameState", state_console_level, state_archive_level)
	Print.create_logger("GroupEvents", events_console_level, events_archive_level)
	Print.create_logger("InputRecorder", input_console_level, input_archive_level)
	Print.create_logger("Game", game_console_level, game_archive_level)
	Print.create_logger("Progression", prgss_console_level, prgss_archive_level)
	Print.create_logger("Spawn", spawn_console_level, spawn_archive_level)
	Print.create_logger("Minerals", mineral_console_level, mineral_archive_level)
	Print.create_logger("Particles", particle_console_level, particle_archive_level)
	# Last so we don't clutter the output with all the loggers we just registered.
	# This one only outputs registered loggers, 
	Print.create_logger("Print", print_console_level, print_archive_level)
	# Our work here is done.
	queue_free()
