extends TextureProgressBar
## Animates the player's progression towards the next level.

## Emitted when [member mineral_count] exceeds [member minerals_to_level_up].
signal level_up

## Defines the width of the X-axis for [member minerals_per_level]. A value of 50 means that after
## 50 levels, we have reached the left side of the curve. Right now, we default to doubling
## [member minerals_to_next_level] for each level once we reach this value.
## [br]
## Setting this to a larger number will make the game easier (we spend more time on small level values).
## Conversely, setting this number to a smaller value will make the game harder.
@export_range(25, 500, 5) var max_intended_levels := 70.0
## X-axis is the current player level. Y-axis is the starting value for [member minerals_to_next_level].
@export var minerals_per_level: Curve
## How long we take to collect one mineral. AKA how fast the progress bar moves.
@export var animate_time := 0.25

## The player's current level.
var current_level := 0
## Total mineral count for this particular level (is compared to [member minerals_to_next_level].
var mineral_count := 0
## Total mineral count for the current play session.
var minerals_collected := 0
## How many minerals we need to collect before leveling up.
var minerals_to_next_level := 10
## Tweener used to animate the progress.
var _tween: Tween
## The base value for a mineral pickup. Mainly changed by debugging things to speed up or slow down gameplay.
var mineral_value := 1

# Logs level progression.
var _log: Logger


func _ready():
	_log = Print.get_logger("Progression")
	reset()


## Resets the node.
func reset():
	current_level = 0
	mineral_count = 0
	minerals_to_next_level = ceil(minerals_per_level.sample(current_level / max_intended_levels))
	max_value = minerals_to_next_level * 100.0
	_log.info("Minerals to next level up: " + str(minerals_to_next_level))
	if _tween:
		_tween.kill()
	_tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	_tween.tween_property(self, "value", 0, animate_time)


## We have reached the next level, reset the counts and increase the target mineral count.
func next_level():
	current_level += 1
	mineral_count = 0
	minerals_to_next_level = ceil(minerals_per_level.sample(current_level / max_intended_levels))
	# Default to doubling.
	if current_level > max_intended_levels:
		minerals_to_next_level *= 2
	max_value = minerals_to_next_level * 100.0
	if _tween:
		_tween.kill()
	_tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	_tween.tween_property(self, "value", 0, animate_time)
	_log.info("Level up! Minerals to next level up: %s" % minerals_to_next_level)


# The game has ended, let's clear everything out.
func _game_over():
	reset()


# Called when the player has selected an upgrade. We should clear the progress bar at this point.
func _on_upgrades_menu_upgrade_selected():
	_log.verbose("Upgrade Selected.")
	next_level()


# Add one mineral towards an upgrade.
func _on_collect_mineral():
	mineral_count += mineral_value
	_log.verbose("Mineral Added, total %s of %s" % [mineral_count, minerals_to_next_level])
	if _tween:
		_tween.kill()
	_tween = get_tree().create_tween().set_pause_mode(Tween.TWEEN_PAUSE_PROCESS)
	_tween.tween_property(self, "value", mineral_count * 100.0, animate_time)
	if mineral_count >= minerals_to_next_level:
		mineral_count = 0
		level_up.emit()
