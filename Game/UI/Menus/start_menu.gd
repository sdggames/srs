extends VBoxContainer

signal start_game(level: int, player: String)

@export var level := 1
@export var player := "DT"
@onready var level_1 = $Levels/Level1
@onready var dart = $Players/Dart

func _game_over():
	level_1.button_pressed = true
	dart.button_pressed = true


func _on_start_pressed():
	start_game.emit(level, player)

func _on_level_1_pressed():
	level = 1


func _on_level_2_pressed():
	level = 2


func _on_dart_pressed():
	player = "DT"


func _on_box_pressed():
	player = "BX"
