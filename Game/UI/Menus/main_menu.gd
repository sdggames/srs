extends CenterContainer

signal select_player(player: String)
signal set_level(level: int)
signal start_pressed()

@onready var main = $Main
@onready var settings = $Settings
@onready var start = $Start


# Make sure that we are reset before the menu is needed.
func _game_over():
	main.visible = true
	start.visible = false


func _on_start_pressed():
	main.visible = false
	start.visible = true


func _on_start_start_game(level, player):
	set_level.emit(level)
	select_player.emit(player)
	start_pressed.emit()


func _on_settings_pressed():
	Print.from("GameState", "Opening Settings Menu.", Print.DEBUG)
	main.visible = false
	settings.visible = true


func _on_exit_pressed():
	Print.from("GameState", "Quit pressed. Exiting application.", Print.INFO)
	get_tree().quit()


func _on_settings_exit():
	Print.from("GameState", "Closing Settings Menu.", Print.DEBUG)
	main.visible = true
	settings.visible = false
