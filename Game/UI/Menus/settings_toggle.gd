class_name SettingsToggle extends Button

@export var setting_name := "Setting"

var _active := true


func _ready():
	_load_data()


## Override this to set the actual thing in the world. Also called on _ready().
func toggle(on: bool):
	pass


func _load_data():
	# Only load the value if it already exists. Otherwise, stick with default.
	var value = Global.load_other(Global.SETTINGS, setting_name)
	if value != null and value is bool:
		_active = value
		toggle(_active)
		text = setting_name + " ON" if value else setting_name + " OFF"


func _on_pressed():
	_active = !_active
	Global.save_other(Global.SETTINGS, setting_name, _active)
	# Use the existing groups to make sure that this change is consumed.
	get_tree().call_group("SaveLoad", "_load_data")
