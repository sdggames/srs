extends CenterContainer

signal upgrade_selected

func _on_upgrade_selected_pressed():
	upgrade_selected.emit()
