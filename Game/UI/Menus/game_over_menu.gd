extends CenterContainer

signal return_to_main_menu


func _on_return_to_menu_pressed():
	return_to_main_menu.emit()
