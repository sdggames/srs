extends SettingsToggle

@export var audio_bus_name := "Master"


func toggle(on: bool):
	AudioServer.set_bus_mute(AudioServer.get_bus_index(audio_bus_name), !on)
