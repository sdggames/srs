extends MarginContainer
## Displays debug info for the game. 

signal level_up
signal player_died
signal start_recording
signal start_playback
signal cancel_recording
signal recorder_file_changed(text: String)

@export var player_level_manager := ^"../PlayerLevelProgress"
@export var toggle_button_modulate_color: Color

var _log: Logger
var _level_manager
# Place for storing temporary values when toggling things in the game.
var _scratchpad := {}

# All overlays.
@onready var overlays := [$LeftOverlay, $RightOverlay]
@onready var toggle = $ShowHide/Toggle
@onready var show_hide = $ShowHide

# Spawn Counts
@onready var asteroid_spawn_counts = $LeftOverlay/AsteroidSpawnCounts
@onready var enemy_spawn_counts = $LeftOverlay/EnemySpawnCounts

# Replay system
@onready var replay = $RightOverlay/Replay
@onready var replay_label = $RightOverlay/Replay/Label/Label
@onready var replay_file = $RightOverlay/Replay/Label/ReplayFile
@onready var record_replay = $RightOverlay/Replay/Select/RecordReplay
@onready var play_replay = $RightOverlay/Replay/Select/PlayReplay
@onready var stop_replay = $RightOverlay/Replay/Select/Stop
@onready var _recording_file = replay_file.text

# Player Details
@onready var mineral_counts = $RightOverlay/MineralCounts
@onready var disable_leveling = $RightOverlay/DisableLeveling
@onready var invincibility = $RightOverlay/Invincibility
@onready var player_pos = $RightOverlay/PlayerPos


func _ready():
	_log = Print.get_logger("Debugging")
	_log.debug("Player level manager is at location: %s" % player_level_manager)
	_level_manager = get_node(player_level_manager)
	
	visible = Global.load_data("debugging_overlay_visible") == true
	Global.register_command("toggle_debugging_overlay", self, "_toggle_visibility", \
			"Turns the debugging overlay on or off.")
	
	# Just get rid of this, no need to wait.
	if Global.replay_mode == InputRecorder.RecorderState.DISABLED:
		replay.queue_free()
	# Turn all of this off if we are playing from a file from the start - we might be recording.
	elif Global.replay_mode >= InputRecorder.RecorderState.PLAYING:
		visible = false
	
	if not is_instance_valid(_level_manager):
		_log.error("Level manager was not found!")


# Update the player level and minerals collected.
func _process(_delta):
	mineral_counts.text = \
	 """Player level:             %s
		Minerals Collected:       %s
		Minerals to next level:   %s
		Total Minerals Collected: %s""" % [
			_level_manager.current_level,
			_level_manager.mineral_count,
			_level_manager.minerals_to_next_level,
			_level_manager.minerals_collected,
		]
	player_pos.text = "Player Pos: %00d, %00d" % [Global.player_pos.x, Global.player_pos.y]


# Update spawn counts. Triggered by a signal.
func _asteroids_spawn_counts(string):
	asteroid_spawn_counts.text = "Spawn Counts:\n" + string


# Update spawn counts. Triggered by a signal.
func _enemies_spawn_counts(string):
	enemy_spawn_counts.text = string


# Toggle visible on the top node.
func _toggle_visibility():
	visible = !visible
	Global.save_data("debugging_overlay_visible", visible)
	# Make sure that we aren't still hidden from button presses.
	if visible:
		_show()


# Turns on the menus.
func _show():
	for o in overlays:
		o.visible = true
	toggle.text = "Hide"
	show_hide.modulate = Color.WHITE
	_log.info("Debugging overlay activated.")


# Turns off the menus, but leaves a button to show. If darken is true, sets the alpha to make the buttons more faint.
func _hide(darken := false):
	for o in overlays:
		o.visible = false
	toggle.text = "Show"
	if darken:
		show_hide.modulate = toggle_button_modulate_color
		_log.info("Debugging overlay hidden. The 'Show' button has been darkened, but is still there." + \
				" Press 'X' again to completely disable overlay.")
	else:
		_log.info("Debugging overlay hidden.")


func _on_level_up_pressed():
	if Global.game_state == Global.GameState.PLAYING:
		level_up.emit()


func _on_kill_player_pressed():
	if Global.game_state == Global.GameState.PLAYING:
		player_died.emit()


func _on_invincibility_pressed():
	if Global.player.invincible:
		Global.player.invincible = false
		invincibility.text = "Invincible: No "
	else:
		Global.player.invincible = true
		invincibility.text = "Invincible: Yes"


func _on_disable_leveling_pressed():
	if _level_manager.mineral_value == 0:
		_level_manager.mineral_value = _scratchpad["Mineral Value"]
		disable_leveling.text = "Skip Level Up: No "
	else:
		_scratchpad["Mineral Value"] = _level_manager.mineral_value
		_level_manager.mineral_value = 0
		disable_leveling.text = "Skip Level Up: Yes"


func _on_record_replay_pressed():
	# Make sure that we aren't accidentally calling from a script.
	if record_replay.visible:
		replay_file.text = _recording_file
		start_recording.emit()


func _on_play_replay_pressed():
	replay_file.text = _recording_file
	start_playback.emit()


func _on_stop_pressed():
	cancel_recording.emit()


func _on_replay_file_text_submitted(new_text):
	_log.info("Changed recording file. Will record to or play from " + new_text)
	_recording_file = new_text
	replay_file.release_focus()
	recorder_file_changed.emit(new_text)


func _on_input_recorder_state_changed(state):
	match state:
		InputRecorder.RecorderState.IDLE:
			replay_label.text = "Replay:"
			record_replay.visible = true
			play_replay.visible = true
			stop_replay.visible = false
		InputRecorder.RecorderState.RECORDING:
			replay_label.text = "Rec:"
			record_replay.visible = false
			play_replay.visible = false
			stop_replay.visible = true
			stop_replay.text = " Stop Recording "
		InputRecorder.RecorderState.PLAYING:
			replay_label.text = "Play:"
			record_replay.visible = false
			play_replay.visible = false
			stop_replay.visible = true
			stop_replay.text = " Stop Replay "


func _on_hide_pressed():
	if toggle.text == "Show":
		_show()
	else:
		_hide()


func _on_completely_close_pressed():
	if show_hide.modulate == Color.WHITE:
		_hide(true)
	else:
		# Turns off the menu permanently. Saves the change to disk.
		_log.warning("Closing debugging overlay and hint. Use the console to open this again.")
		_toggle_visibility()


func _on_game_data_select_pressed(file_number):
	_log.info("Switching to game settings file: " + Global.ALT_SAVE_FILES[file_number])
	Global.skip_save = false
	Global.change_save_file_location(Global.ALT_SAVE_FILES[file_number])


func _on_disable_save_pressed():
	_log.warning("You just disabled saving! Nothing will be written to file until you select a valid save file to write to!")
	Global.skip_save = true
