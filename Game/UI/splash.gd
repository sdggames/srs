extends Node2D

const game_scene := "res://Game/game.tscn"


func _ready():
	ResourceLoader.load_threaded_request(game_scene)
	# Test for false because we will default to null (if the key was never set)
	if Global.load_data("Splash_Enabled") == false:
		Print.from("Game", "Skipping splash screen.", Print.DEBUG)
		# Flash super quickly while loading the main scene.
		await get_tree().create_timer(0.5).timeout
		get_tree().change_scene_to_packed(ResourceLoader.load_threaded_get(game_scene))
	else:
		Print.from("Game", "Starting splash screen.", Print.DEBUG)
		$AnimationPlayer.play("splash")


func _on_animation_player_animation_finished(_anim_name):
	Print.from("Game", "Splash ended, switching scenes.", Print.DEBUG)
	get_tree().change_scene_to_packed(ResourceLoader.load_threaded_get(game_scene))
