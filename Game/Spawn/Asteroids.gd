extends Spawner
## [Spawner] for asteroids.
##
## Spawns all asteroids, meteors, or other non-enemy units.
## When adding a unit, make sure to do the following:
## [br]    1. Update [enum AutoloadGlobal.AsteroidTypes] with the new asteroid type.
## [br]    2. Add a prefab to the [member AsteroidPrefabs] array.
## [br]    3. Modify this script to include an additional minimum count value and add to 
## the array in [method _get_min_counts].
## [br]    4. Modify this script to include an additional impulse function that calls
## [method Spawner.spawn_impulse] with the [enum AutoloadGlobal.AsteroidTypes] value and [param count].
## [br]    5. Update the LevelAnimation object to set the new min count and call the impulse function.

@export_group("Minimum Counts")
@export var asteroid_0_min : int = 0
@export var asteroid_1_min : int = 0
@export var asteroid_2_min : int = 0
@export var asteroid_3_min : int = 0
@export var asteroid_4_min : int = 0
@export var asteroid_5_min : int = 0
@export var asteroid_6_min : int = 0
@export var asteroid_7_min : int = 0
@export var asteroid_8_min : int = 0
@export var asteroid_9_min : int = 0

@export_group("", "")
@export var AsteroidPrefabs: Array[PackedScene]

func asteroid_0_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_0)
func asteroid_1_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_1)
func asteroid_2_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_2)
func asteroid_3_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_3)
func asteroid_4_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_4)
func asteroid_5_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_5)
func asteroid_6_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_6)
func asteroid_7_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_7)
func asteroid_8_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_8)
func asteroid_9_impulse(count: int): spawn_impulse(count, Global.AsteroidTypes.ASTEROID_9)


func _ready():
	object_names = Global.AsteroidTypes.keys()
	unpack(AsteroidPrefabs)
	super._ready()


## Get the parameters from the Asteroid class, and appeand to the defaults for generic objects.
func get_default_params() -> Dictionary:
	var params = Asteroid.get_asteroid_params()
	params.merge(super.get_default_params(), false)
	return params


## Spawn more asteroids. Each asteroid has a list of smaller asteroids to spawn. Also drop one mineral.
func _object_destroyed(_type, object: Asteroid):
	if (object.break_asteroids.size() > 0):
		for i in object.break_asteroids:
			var params = get_default_params()
			params["position"] = object.position
			params["linear_velocity"] += object.linear_velocity
			spawn_object.emit(i, Global.uuid, params)
	minerals.spawn_mineral(object.position)


# Return the counts.
func _get_min_counts() -> Array[int]:
	return [asteroid_0_min, asteroid_1_min, asteroid_2_min, asteroid_3_min, asteroid_4_min,
			asteroid_5_min, asteroid_6_min, asteroid_7_min, asteroid_8_min, asteroid_9_min]
