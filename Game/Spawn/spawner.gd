class_name Spawner extends Node2D
## Spawns enemies or asteroids into a scene.
##
## This is a base class for the Asteroids and Enemies scripts. Does all of the common stuff like
## creating objects, freeing objects, etc.
## TODO: Support object pooling and manages performance better...

## Emitted whenever the spawn counts change. Used for debugging.
signal spawn_counts(string: String)

## Emitted each time an object is spawned. Used for replay and synchronization.
signal object_spawned(object, id: int)

## Loopback signal to spawn an object from a recipe. !!ALL RANDOM CALLS MUST HAPPEN BEFORE CALLING
## THIS SO CALLS CAN BE REPEATED!!
signal spawn_object(index: int, uid: int, params: Dictionary)

## Minimum character width for the debug string. Ensures that everything is aligned.
const debug_pad_width = 10

## A reference to the mineral parent. This needs to be a sibling to the Spawner node.
@onready var minerals = $"../Minerals"
## A reference to the particle parent. This needs to be a sibling to the Spawner node.
@onready var particles = $"../Particles"

## The name for each enemy type. Should be set to AsteroidTypes.keys() or Enemies.keys() by the
## child of this class. THIS MUST BE SET BY THE CHILD CLASS.
var object_names: Array

## An array of objects to create. THIS MUST BE SET BY THE CHILD CLASS.
var prefabs: Array

# All the minimum counts in the order that they are referenced by the enum.
var _min_counts: Array[int]

# The unpacked scenes.
var _object_scenes: Array[PackedScene]

var _log: Logger

# A list of each active object by type.
var _current_object_count: Array[int]


func _ready():
	_log = Print.get_logger("Spawn")
	_min_counts = _get_min_counts()
	_current_object_count.resize(object_names.size())
	_log.assert_that(not _min_counts.is_empty(), "Child class forgot to set min_counts!")
	_log.assert_that(not object_names.is_empty(), "Child class forgot to set object_names!")
	_log.assert_that(_min_counts.size() == object_names.size(), \
			"Spawner doesn't have the same number of min_counts as the Global enum!")
	_log.assert_that(not _object_scenes.is_empty(), \
			"Child class forgot to call unpack on the object scene array!")
	_log.assert_that(_object_scenes.size() == object_names.size(), \
			"The number of objects does not match the number of expected objects!")


func _physics_process(_delta):
	# Only spawn things if the game is active.
	if Global.game_state == Global.GameState.PLAYING:
		# Update the _min_counts before we use it.
		_min_counts = _get_min_counts()
		
		# Update the debugging interface with the latest counts.
		spawn_counts.emit(_build_debug_string())
		
		for i in _min_counts.size():
			while _current_object_count[i] < _min_counts[i]:
				spawn_object.emit(i, Global.uuid, get_default_params())


## Spawns [param count] number of objects at [param index]. The [param index] is used to index into
## the [member prefabs] array.
func spawn_impulse(count: int, index: int):
	_log.debug("Impulse triggered. Spawning %s objects of type %s" % [count, object_names[index]])
	for x in count:
		spawn_object.emit(index, Global.uuid, get_default_params())


## Spawns a single object of type [param index]. Object will be added to the scene on the next frame.
func _on_spawn_object(index: int, id: int, params: Dictionary) -> Node2D:
	_current_object_count[index] += 1
	_log.verbose("Object of type %s created. Object count is %s" % [index, _current_object_count[index]])
	
	var obj: Node2D = _object_scenes[index].instantiate()
	obj.destroyed.connect(_on_object_destroyed.bind(index, obj))
	obj.free_ready.connect(_on_object_free_ready.bind(index, obj))
	
	for p in params.keys():
		obj.set(p, params[p])
	
	object_spawned.emit(obj, id)
	add_child.call_deferred(obj)
	return obj


## Function for child class to override or expand. All of the random calls need to occur BEFORE
## sending the spawn_object signal. Use this function to add properties to be set when the object
## is created.
func get_default_params() -> Dictionary:
	return {"position": Global.screen_wrap.random_spawn_pos()}


## Function for the child class to override. Called whenever an object is destroyed.
## The object is automatically cleaned up, but use this opportunity to spawn more things, or
## react to the destruction of a huge boss or something.
func _object_destroyed(_type, _object):
	pass


## Grab the objects to initialize. If they are packed, we may want to load them at this time.
## We are assuming that the object order matches the Global enum, and there is no way to test this.
func unpack(objects: Array[PackedScene]):
	_object_scenes = objects


## All the minimum counts in the order that they are referenced by the enum.
## THIS MUST BE OVERRIDDEN BY THE CHILD CLASS.
func _get_min_counts() -> Array[int]:
	_log.error("_get_min_counts must be overridden by the child class!")
	return []


## Assemble a string to pass to the Debugging Overlay.
func _build_debug_string() -> String:
	var s = ""
	for i in object_names.size():
		s += "%s:%3d of%3d\n" % [object_names[i].rpad(debug_pad_width), _current_object_count[i], _min_counts[i]]
	return s


# Object was destroyed by player. Respond appropriately.
func _on_object_destroyed(type, object):
	_log.verbose("Object of type %s destroyed. Object count is %s" % [type, _current_object_count[type]])
	_object_destroyed(type, object)


# Object is ready to leave the scene tree. Update our count, then pool or free.
func _on_object_free_ready(type, object):
	_current_object_count[type] -= 1
	# TODO: Pool object instead of destroying!
	object.queue_free()


## Everything is invisible, so let's clean up our references.
func _game_reset():
	# Reset all counts.
	_current_object_count.fill(0)
	# Destroy, we are not pooling at this point.
	for child in get_children():
		child.queue_free()
