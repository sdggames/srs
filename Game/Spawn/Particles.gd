extends Node2D

const PLAYER_DIE = preload("res://Game/Effects/player_die.tscn")


func spawn_particle():
	pass


func _player_killed():
	var explosion = PLAYER_DIE.instantiate()
	explosion.global_position = Global.player_pos
	add_child.call_deferred(explosion)
