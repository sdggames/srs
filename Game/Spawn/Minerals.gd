extends Node2D

@export var mineral_scene: PackedScene

var _log: Logger


func _ready():
	_log = Print.get_logger("Minerals")


## Spawns a mineral.
func spawn_mineral(pos: Vector2):
	_log.verbose("Mineral Spawned at position %s" % pos)
	var mineral: Mineral = mineral_scene.instantiate()
	mineral.position = pos
	add_child.call_deferred(mineral)


func _game_reset():
	for mineral in get_children():
		mineral.queue_free()
