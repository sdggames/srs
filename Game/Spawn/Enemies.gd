extends Spawner
## [Spawner] for enemies.
##
## Spawns all enemy units and bosses.
## When adding a unit, make sure to do the following:
## [br]    1. Update [enum AutoloadGlobal.EnemyUnitTypes] with the new enemy type.
## [br]    2. Add a prefab to the [member EnemyPrefabs] array.
## [br]    3. Modify this script to include an additional minimum count value and add to 
## the array in [method _get_min_counts].
## [br]    4. Modify this script to include an additional impulse function that calls
## [method Spawner.spawn_impulse] with the [enum AutoloadGlobal.EnemyUnitTypes] value and [param count].
## [br]    5. Update the LevelAnimation object to set the new min count and call the impulse function.

@export_group("Minimum Counts")
@export var enemy0_min : int = 0

@export_group("", "")
@export var EnemyPrefabs: Array[PackedScene]

func enemy0_impulse(count: int): spawn_impulse(count, Global.EnemyUnitTypes.ENEMY_0)


func _ready():
	object_names = Global.EnemyUnitTypes.keys()
	unpack(EnemyPrefabs)
	super._ready()


func _get_min_counts() -> Array[int]:
	return [enemy0_min]
