class_name Mineral extends RigidBody2D

@onready var on_screen = $ObjectWrap


## Delete extra minerals if they are out of frame. Delete after 10 seconds, retry every 10 seconds.
func _on_timer_timeout():
	if !on_screen.is_on_screen():
		queue_free()
