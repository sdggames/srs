extends Node2D


## A list of player ships to instance when starting the game.
const player_ships := {
	"DT": preload("res://Game/Player/Ships/dart.tscn"), # Dart.
	"BX": preload("res://Game/Player/Ships/box.tscn"), # Box.
}

## For player death particles.
@onready var particles = $Particles

## Reference to the game.
@export var game = ^".."
@export var player_level_progress = ^".."

## Which ship are we going to use?
@export var player_ship := "DT"

## Reference to the player object.
var player: Player


# Move this when the game starts. It is offset to make things easier in the editor.
func _ready():
	global_position = Vector2.ZERO
	game = get_node(game)
	player_level_progress = get_node(player_level_progress)


func _select_player(ship):
	player_ship = ship


## Called before the level fade-in transition.
func _game_start():
	player = player_ships[player_ship].instantiate()
	player.died.connect(game._on_player_died)
	player.collected_mineral.connect(player_level_progress._on_collect_mineral)
	add_child(player)


## Called when the game has successfully transitioned to the game menu. All play elements are hidden and can be destroyed.
func _game_reset():
	if player:
		var p = player
		player = null
		p.queue_free()
