extends Node2D

@onready var stars_1 = $Stars1
@onready var stars_2 = $Stars2

## Backgrounds to select when starting a level.
@onready var backgrounds := {
	1: stars_1,
	2: stars_2,
}

func _set_level(level):
	for b in backgrounds.values():
		b.visible = false
	
	backgrounds[level].visible = true


func _game_over():
	_set_level(1)
