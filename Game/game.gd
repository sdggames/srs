class_name Game extends Node
## Main game script. I'm trying to delegate so this doesn't get huge.
## @tutorial(Writing Godot Documentation): https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_documentation_comments.html
## @tutorial(Godot Code Style Guide): https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_styleguide.html#naming-conventions

const SETTINGS = &"Settings"

## Is the game paused?
var game_paused: bool:
	get:
		return get_tree().paused
	set(value):
		_log.verbose("Pausing Gameplay" if value else "Resuming Gameplay")
		get_tree().paused = value

## The state of the game. This value is copied to the [member AutoloadGlobal.game_state] for ease of access.
## When the state changes, an appropriate call will go out to any node in the [param GameEvents] group.
var game_state: AutoloadGlobal.GameState:
	get:
		return _state
	set(value):
		if _state != value:
			_game_events_state_change(value, _state)
		_state = value

# Private game state, used by game_state
var _state := Global.GameState.BOOT
var _log: Logger

@onready var transition_manager = $TransitionManager
@onready var background = $Background
@onready var screen_effects = $ScreenEffects


func _ready():
	# Technically, I shouldn't be setting a private var, but I don't want this showing up in
	# autocomplete results all across the project. Sue me...
	Global._game = self
	_log = Print.get_logger("Game")
	_log.verbose("Game Start. Transitioning to main menu.")
	transition_manager.play("MainMenuFadeIn")
	# Make sure that we are synced with the latest settings everywhere.
	get_tree().call_group("SaveLoad", "_load_data")


func _input(event):
	if event.is_action_pressed("pause"):
		# Toggle pause
		_log.debug("Pause Key Pressed")
		if game_state == Global.GameState.PAUSED:
			transition_manager.play("Unpause")
		# Only pause if the game is running and we aren't already in a menu.
		elif game_state == Global.GameState.PLAYING: # and not transition_manager.is_playing()
			transition_manager.queue("Pause")
			game_state = Global.GameState.PAUSED
			game_paused = true


# Called when the game data has been altered.
func _load_data():
	background.visible = Global.load_other(SETTINGS, "Stars") == true
	screen_effects.visible = Global.load_other(SETTINGS, "Screen Effects") == true


# Call the appropriate group action to warn all notes of a major game state change.
func _game_events_state_change(new_state: AutoloadGlobal.GameState, old_state: AutoloadGlobal.GameState):
	# Log this event to both our class and the GameState debug log.
	var message = "Switching game state from %s to %s" % \
			[Global.GameState.keys()[old_state], Global.GameState.keys()[new_state]]
	_log.verbose(message)
	Print.from("GameState", message, Print.DEBUG)
	match new_state:
		Global.GameState.BOOT:
			pass
		Global.GameState.MAIN_MENU:
			get_tree().call_group("GameEvents", "_game_reset")
		Global.GameState.PLAYING:
			get_tree().call_group("GameEvents", "_player_spawned")
		Global.GameState.UPGRADING:
			pass
		Global.GameState.PAUSED:
			pass
		Global.GameState.GAME_OVER:
			get_tree().call_group("GameEvents", "_game_over")


# Hide the player and make the transition to the game over menu.
func _on_player_died():
	_log.debug("Player Died")
	get_tree().call_group("GameEvents", "_player_killed")
	game_state = Global.GameState.GAME_OVER
	transition_manager.queue("GameOver")


# Open the upgrades menu and present the player with their options.
func _on_level_up():
	if game_state == Global.GameState.PLAYING:
		_log.debug("Level Up")
		game_state = Global.GameState.UPGRADING
		game_paused = true
		transition_manager.queue("OpenUpgrades")


# We finished changing states.
func _on_transition_manager_animation_finished(anim_name):
	_log.verbose("Game transition manager just completed transition: " + anim_name)
	match anim_name:
		"MainMenuFadeIn":
			game_state = Global.GameState.MAIN_MENU
		"StartGame":
			game_state = Global.GameState.PLAYING
		"OpenUpgrades":
			pass
		"CloseUpgrades":
			game_state = Global.GameState.PLAYING
		"GameOver":
			game_state = Global.GameState.MAIN_MENU
		"MainMenuReturn":
			pass
		"Pause":
			pass # Game is paused when we start the animation.
		"Unpause":
			game_paused = false
			game_state = Global.GameState.PLAYING # The only state we can pause from.
		_:
			pass


# Close the start menu and start the game.
func _on_main_menu_start_pressed():
	_log.debug("Start Pressed")
	# We are in the menu right now.
	if !transition_manager.is_playing():
		# Call now so we can set up the game state before the fade-in.
		get_tree().call_group("GameEvents", "_game_start")
		transition_manager.play("StartGame")
	
	# We are in a valid transition, but haven't completed it yet.
	elif game_state == Global.GameState.BOOT:
		# Force the transition. Setter might send a signal.
		game_state = Global.GameState.MAIN_MENU
		# Continue as expected.
		get_tree().call_group("GameEvents", "_game_start")
		transition_manager.play("StartGame")


# Player picked an upgrade. We should go back to the main game scene.
func _on_upgrades_menu_upgrade_selected():
	_log.debug("Upgrade Selected.")
	game_paused = false
	transition_manager.play("CloseUpgrades")


# Close the game over screen and open the main menu instead.
func _on_game_over_menu_return_to_main_menu():
	_log.debug("Closed GameOver Menu")
	transition_manager.queue("MainMenuReturn")


# Close the pause menu and resume the game.
func _on_pause_menu_resume():
	_log.debug("Game Resumed")
	transition_manager.play("Unpause")
