@tool
class_name VectorLine2D extends Line2D
## A line2D with a selectable thickness and built-in gradient.
##
## This class is a tool script that has five styles (four standard, plus default).
## When the node is created in the editor or the [member line_style] is changed,
## the line weight and gradient texture will update automatically.
## [br][br]
## The class also has support for simple animations through the [member subdivide_length] and
## [member percent_drawn] values.

## What thickness and texture should we use when defining this vector line?
enum LineStyles {
	LINE_1, ## Very Thin
	LINE_2, ## Thin
	LINE_3, ## Medium
	LINE_4, ## Thick
	UNTEXTURED, ## No gradient.
	CUSTOM, ## Vector line has been modified, don't update it.
}

# How thick the vector line is.
const _line_weights = {
	LineStyles.LINE_1: 1,
	LineStyles.LINE_2: 2,
	LineStyles.LINE_3: 4,
	LineStyles.LINE_4: 6,
	LineStyles.UNTEXTURED: 4,
}

# The texture to use for this particular vector line.
const _line_textures = {
	LineStyles.LINE_1: preload("res://Game/Art/vector_line_1.tres"),
	LineStyles.LINE_2: preload("res://Game/Art/vector_line_2.tres"),
	LineStyles.LINE_3: preload("res://Game/Art/vector_line_3.tres"),
	LineStyles.LINE_4: preload("res://Game/Art/vector_line_4.tres"),
	LineStyles.UNTEXTURED: null,
}

# The gradient texture to use to end the line with a smooth fade instead of an ugly rounded bit.
const _line_cap_gradient = preload("res://Game/Art/vector_line_cap.tres")

## Which style should we use? This is a tool script, so it update in the editor when changed.
## [br]
## Technically, you could change at runtime too, if there is a use for that.
@export var line_style := LineStyles.UNTEXTURED:
	set(value):
		line_style = value
		if Engine.is_editor_hint():
			_update_line_style()

## Defines the maximum length for a line segment in pixels. A value of zero will be ignored. Any
## updates will be applied to the [member points] array during the [method _ready] call during
## runtime, or when [member subdivide] is set in the editor.
@export_range(0, 1028, 1) var subdivide_length: int = 0

## Flag to subdivide the points in the editor. Stays false, but applies [member subdivide_length] to
## the [member points] array.
@export var subdivide := false:
	set(value):
		subdivide = false
		if subdivide_length > 0:
			_subdivide_points()

## Flag to smoothly cap the line. Right now, this just sets [member gradient] to a gradient with
## transparent ends. TODO: it is up to the dev to add extra points on the end. Maybe I should
## add some code to put the two points in automatically.
@export var smooth_ends := false:
	set(value):
		smooth_ends = value
		gradient = _line_cap_gradient if value else null

## Overrides the [member width_curve] value to hide a portion of the line. Point ordering matters,
## this will draw from the first point to the end of the points array.
## [br][br]
## NOTE: Godot will set a linear gradient across each line segment. Use [member subdivide_length] to
## ensure that the taper isn't too noticable.
@export_range(0, 100, 0.1) var percent_drawn := 100.0:
	set(value):
		# Draw the value.
		percent_drawn = value
		var curve = Curve.new()
		var percent_point = value / 100.0
		if percent_point < 1.0:
			curve.add_point(Vector2(percent_point, 0))
		if percent_point > 0.0:
			curve.add_point(Vector2(percent_point, 1))
		width_curve = curve


# Sets the new line style to the constant value for width and texture.
func _update_line_style():
	if line_style < LineStyles.CUSTOM:
		width = _line_weights[line_style]
	
		texture = _line_textures[line_style]
		texture_mode = Line2D.LINE_TEXTURE_STRETCH
		
		joint_mode = Line2D.LINE_JOINT_ROUND
		begin_cap_mode = Line2D.LINE_CAP_ROUND
		end_cap_mode = Line2D.LINE_CAP_ROUND


# Add points into the points array to make animations smoother.
func _subdivide_points():
	var new_points = PackedVector2Array()
	var old_points = points
	# If we are closed, duplicate to make sure we add points on the final segment.
	if closed:
		old_points.append(old_points[0])
	if points.size() > 0:
		new_points.append(old_points[0]) # Always add the first point
		
		for i in range(1, old_points.size()):
			var start_point = old_points[i - 1]
			var end_point = old_points[i]
			var segment_length = start_point.distance_to(end_point)
			
			if segment_length > subdivide_length:
				# Number of new segments needed, rounded up
				var num_new_segments = ceil(segment_length / subdivide_length)
				for j in range(0, num_new_segments):
					var t = float(j) / num_new_segments
					# Linearly interpolate new points along the segment
					var new_point = start_point.lerp(end_point, t)
					new_points.append(new_point)
			else:
				# If the segment is shorter than the max, just add the end point
				new_points.append(end_point)
		# Add the final point, our loop doesn't include the end point.
		if not closed:
			new_points.append(old_points[-1])
		points = new_points


# Only update once if we are starting with the Line2D default. Otherwise, ignore user changes.
func _ready():
	if !Engine.is_editor_hint() and subdivide_length > 0:
		_subdivide_points()
