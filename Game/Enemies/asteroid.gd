class_name Asteroid extends RigidBody2D

## Echo hit method for puppeteer
signal on_hit
## Emitted when the asteroid health reaches zero. Asteroid is emitting death particles.
signal destroyed
## Emitted once the death animation is complete. Asteroid is ready to be destroyed or pooled.
signal free_ready

## The size of the smallest asteroid. This needs to match the size of the points array in [member asteroid_shapes].
const min_radius = 32.0
const start_speed_range := Vector2(100, 200)
const angular_velocity_range := Vector2(-90, 90)

## What asteroid(s) this will break into when destroyed. Used to index into the asteroid array.
@export var break_asteroids: Array[int] = [0]

## The total hit points for this asteroid. Is reduced when [method hit] is called.
@export var health := 1

## Half the width of this asteroid.
@export var radius := 32.0

## A collection of shapes to use.
@export var asteroid_shapes: AsteroidShape

## Unique to this asteroid.
var randx: int

# Each of the collision masks for re-enabling.
var _masks

@onready var collision_shape = $CollisionShape
@onready var shape = $Shape
@onready var explosion = $Explosion
@onready var object_wrap = $ObjectWrap


static func get_asteroid_params() -> Dictionary:
	var d = {}
	d["linear_velocity"] = Vector2(randf() - 0.5, randf() - 0.5).normalized() * randf_range(start_speed_range.x, start_speed_range.y)
	d["angular_velocity"] = deg_to_rad(randf_range(angular_velocity_range.x, angular_velocity_range.y))
	d["randx"] = Global.last_uuid
	return d


func _ready():
	shape.points = asteroid_shapes.get_random_scaled_shape(radius / min_radius, randx)
	object_wrap.set_radius(radius)
	
	if collision_shape is CollisionShape2D:
		collision_shape.shape.radius = radius
	elif collision_shape is CollisionPolygon2D:
		collision_shape.polygon = shape.points


func disable():
	shape.visible = false
	_masks = [collision_layer, collision_mask]
	collision_layer = 0
	collision_mask = 0


func enable():
	shape.visible = true
	collision_layer = _masks[0]
	collision_mask = _masks[1]


## Take damage. If damage is greater than remaining HP, then destroy. Returns kinetic damage taken.
func hit(energy_damage: int, kinetic_damage: int, blast_damage: int) -> int:
	@warning_ignore("integer_division")
	var base_damage = (energy_damage / 2) + blast_damage
	if base_damage >= health: # We died
		disable.call_deferred()
		destroyed.emit()
		explosion.start()
		return 0
	else:
		health -= base_damage
	if kinetic_damage >= health:
		disable.call_deferred()
		destroyed.emit()
		explosion.start()
		return kinetic_damage - health
	else:
		health -= kinetic_damage
		return kinetic_damage


func _on_explosion_animation_finished():
	free_ready.emit()
