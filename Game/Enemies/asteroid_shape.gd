class_name AsteroidShape extends Resource

## A list of multiple asteroid shapes. Each shape is a Vector2 Array with a width of 64 pixels.
@export var asteroid_shapes: Array[PackedVector2Array]

## Returns one of the shapes from the asteroid shapes array.
func get_random_shape(random):
	return asteroid_shapes[random % asteroid_shapes.size()]


## Picks a random shape, then scales it by [param scale_factor].
func get_random_scaled_shape(scale_factor, random):
	var points = get_random_shape(random)
	var scaled_points = []
	for p in points:
		scaled_points.append(p * scale_factor)
	return scaled_points
