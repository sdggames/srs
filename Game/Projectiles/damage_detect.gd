class_name DamageDetect extends Area2D
## Detect when a bullet, missile, etc. hits a target.
## 
## Calls the target's hit function and calculates leftover damage values.
## If there is leftover kinetic damage or blast damage, 

signal damage_done
signal destroy

## One-shot damage. All energy is transferred into the first target.
## Energy damage does half damage to asteroids.
@export var damage_energy: int = 0

## Leftover damage energy is conserved for the next target. The destroy signal is only sent
## once kinetic damage reaches zero.
@export var damage_kinetic: int = 0

## Blast damage is a constant value for all affected tergets.
@export var damage_blast: int = 0


## Overridable function to do damage to a target. If 
func do_damage(body):
	damage_kinetic -= body.hit(damage_energy, damage_kinetic, damage_blast)
	if damage_kinetic + damage_blast == 0:
		destroy.emit()
	else:
		damage_energy = 0
		damage_done.emit()


## We use collision layers, so this body is an asteroid or an enemy. No need to check.
func _on_body_entered(body):
	do_damage(body)
