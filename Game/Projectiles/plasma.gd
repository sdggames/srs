extends RigidBody2D
# TODO: Create more structure with bullets, have shared base class(es) and make sure to handle wrapping
# I should think about this a bit more before diving in with weapons.


## The minimum rotation of the bullet in degrees per second.
@export var min_rotation_speed := 200
## The maximum rotation of the bullet in degrees per second.
@export var max_rotation_speed := 400


func _ready():
	if randi_range(0, 1) == 0:
		angular_velocity = deg_to_rad(randi_range(min_rotation_speed, max_rotation_speed))
	else:
		angular_velocity = -deg_to_rad(randi_range(min_rotation_speed, max_rotation_speed))


func _on_damage_detect_destroy():
	queue_free()
