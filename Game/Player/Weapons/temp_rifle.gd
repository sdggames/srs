extends Node2D

## The speed of the projectile.
@export var bullet_speed := 500
@export var bullet_prefab: PackedScene
@export var player_path := ^".."
@export var shots_per_cascade := 4

@onready var shoot_audio = $ShootAudio
@onready var reload_timer = $ReloadTimer
@onready var bullets = $Bullets

var player: CharacterBody2D

var cascade_count := 0
var armed := false
var loaded := false


var velocity: Vector2:
	get:
		return player.velocity


func _ready():
	player = get_node(player_path)
	reload_timer.start()


# Oh my goodness, this code is so ugly that it makes my eyes bleed! Sure, it looks like I made a modular
# and expandable system, but on a second glance, it's obvious that I just copy-pasted from another
# project. Nothing makes sense, and things are broken into smaller functions arbitrarily. The only
# solution is to call this class "Temporary Rifle" and spend more than five minutes on the weapons
# system when I do this a second time...


func shoot():
	if Global.game_state == Global.GameState.PLAYING:
		var bullet = new_bullet()
		bullet.global_position = global_position
		bullet.linear_velocity += forward() * bullet_speed + Vector2(randf_range(-25, 25), randf_range(-25, 25))
		register_bullet(bullet)
		shoot_audio.play()


# Get the forward vector.
func forward() -> Vector2:
	return global_transform.x.normalized()


## Creates a new bullet. The bullet does not exist in the scene until register_bullet is called.
## The bullet's starting velocity will match the player's velocity.
func new_bullet():
	var bullet = bullet_prefab.instantiate()
	bullet.linear_velocity = velocity
	return bullet


## Add the bullet to the scene. Call this once you are done setting velocity, etc.
func register_bullet(bullet):
	bullets.add_child(bullet)


func _on_reload_timer_timeout():
	cascade_count = shots_per_cascade
	armed = true


func _music_tick(note):
	if Global.game_state == Global.GameState.PLAYING and armed and note % 4 == 0:
		shoot()
		reload()


func reload():
	loaded = false
	armed = false
	cascade_count -= 1
	if cascade_count > 0:
		loaded = true
	else:
		reload_timer.start()


## Called once per sixteenth note. Use the parameters to determine which partial note this is.
func _music_partial_tick(_whole: bool, quarter: bool, _eighth: bool):
	if loaded and quarter:
		shoot()
		reload()
