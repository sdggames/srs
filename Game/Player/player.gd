class_name Player extends CharacterBody2D
## Basic Player object
##
## TODO: Update and extend this class when doing the weapons system refactor.

signal died
signal collected_mineral

const INT_MAX = 2 ** 63 - 1

@export var speed := 250
@export var rotation_speed := 2
@export var friction := 0.25
@onready var thruster = $Thruster/Anim

var invincible := false

var thrusting := false:
	set(value):
		if thrusting != value:
			thrusting = value
			thruster.play("Thrust" if thrusting else "RESET")

var _start_pos: Vector2


func _ready():
	Global.player = self
	_start_pos = global_position


func _physics_process(delta):
	if Global.replay_mode == InputRecorder.RecorderState.PLAYING:
		return
	# Rotate
	var rot = Input.get_axis("left", "right")
	var add_velocity = Vector2.ZERO
	
	# Thrust
	if Input.is_action_pressed("thrust"):
		if Global.game_state == Global.GameState.PLAYING:
			add_velocity = transform.x.normalized() * speed * delta
			if !thrusting:
				thrusting = true
	elif thrusting:
		thrusting = false
	
	rotation += rot * delta * rotation_speed
	
	# Friction
	velocity = (velocity * (1 - (friction * delta))) + add_velocity
	
	screen_wrap()
	move_and_slide()


func screen_wrap():
	if global_position.x > Global.screen_wrap.player_wrap.right:
		global_position.x -= Global.screen_wrap.player_wrap.width
	elif global_position.x < Global.screen_wrap.player_wrap.left:
		global_position.x += Global.screen_wrap.player_wrap.width
	if global_position.y > Global.screen_wrap.player_wrap.bottom:
		global_position.y -= Global.screen_wrap.player_wrap.height
	elif global_position.y < Global.screen_wrap.player_wrap.top:
		global_position.y += Global.screen_wrap.player_wrap.height


func _player_spawned():
	visible = true


func _game_reset():
	global_position = _start_pos
	global_rotation_degrees = 90
	visible = true


func _on_damage_zone_body_entered(body):
	body.hit(0, 0, INT_MAX)
	if visible and !invincible:
		visible = false
		thruster.play("RESET")
		velocity = Vector2.ZERO
		visible = false
		died.emit()


func _on_mineral_detect_body_entered(body):
	collected_mineral.emit()
	body.queue_free()
