extends Node2D


@onready var attractor = $Attractor
@onready var mineral_detect = $MineralDetect

var gravity = 0
var mask = 0


func _ready():
	gravity = attractor.gravity
	mask = mineral_detect.collision_mask


func _player_spawned():
	attractor.gravity = gravity
	mineral_detect.collision_mask = mask


func _player_killed():
	gravity = attractor.gravity
	mask = mineral_detect.collision_mask
	attractor.gravity = 0
	mineral_detect.collision_mask = 0
