class_name ObjectWrap extends VisibleOnScreenNotifier2D
## Helper class that wraps its parent around the screen

# A reference to my parent.
@onready var p = $".."


## Sets the radius. Technically, this is a square, but whatever...
func set_radius(radius: int):
	rect = Rect2(-radius, -radius, radius * 2, radius * 2)


func _physics_process(_delta):
	# Only wrap if we are fully out of view.
	if !is_on_screen():
		_wrap()


# Wrap around. We can add additional wrap types later (delete on exit, etc.)
func _wrap():
	if p.global_position.x > Global.screen_wrap.object_wrap.right:
		p.global_position.x -= Global.screen_wrap.object_wrap.width
	elif p.global_position.x < Global.screen_wrap.object_wrap.left:
		p.global_position.x += Global.screen_wrap.object_wrap.width
	if p.global_position.y > Global.screen_wrap.object_wrap.bottom:
		p.global_position.y -= Global.screen_wrap.object_wrap.height
	elif p.global_position.y < Global.screen_wrap.object_wrap.top:
		p.global_position.y += Global.screen_wrap.object_wrap.height
