class_name WrapWindow extends ReferenceRect
## A window that displays a wrapping zone. Contains functions for getting the top, bottom, etc.


## The top edge of the rectangle.
var top: float:
	get:
		return position.y

## The bottom edge of the rectangle.
var bottom: float:
	get:
		return position.y + size.y

## The right edge of the rectangle.
var right: float:
	get:
		return position.x + size.x

## The left edge of the rectangle.
var left: float:
	get:
		return position.x

## The total height of the rectangle.
var height: float:
	get:
		return size.y

## The total width of the rectangle.
var width: float:
	get:
		return size.x
