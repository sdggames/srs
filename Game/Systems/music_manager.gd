extends Node

@onready var sync_anim = $SyncAnim
@onready var song_1 = $Song1
@onready var visualizer = $Layer/Visualizer

var tick_number := 0
var ending := true


func _ready():
	sync_anim.play("play")
	song_1.play()


func _tick():
	# This can be called twice in a row if we reset the animation right after it reset.
	if ending:
		get_tree().call_group("MusicSync", "_music_partial_tick", true, true, true)
		get_tree().call_group("MusicSync", "_music_tick", tick_number)
		tick_number += 1
		ending = false


func _quarter():
	get_tree().call_group("MusicSync", "_music_partial_tick", false, true, true)


func _eighth():
	get_tree().call_group("MusicSync", "_music_partial_tick", false, false, true)


func _sixteenth():
	get_tree().call_group("MusicSync", "_music_partial_tick", false, false, false)


# Called when the game data has been altered.
func _load_data():
	visualizer.visible = Global.load_other(Global.SETTINGS, "Music Visualizer") == true


## Called when the animation is about to loop. (Used to prevent double-ticks on song change)
func end():
	ending = true


func _on_song_finished():
	tick_number = 0
	ending = true
	song_1.play()
	sync_anim.seek(0)
