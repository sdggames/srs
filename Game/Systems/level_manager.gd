extends Node

const levels := {
	1: "Level_1",
	2: "Level_2",
}

@onready var level_animation = $LevelAnimation
@onready var enemy_animation = $EnemyAnimation

var level: String
var _log: Logger


func _ready():
	_log = Print.get_logger("Spawn")
	level = levels[1]


## Called before the level fade-in transition.
func _game_start():
	_log.debug("Starting both spawn animations. Eventually, the EnemyAnimation should be delayed.")
	if Global.replay_mode != InputRecorder.RecorderState.PLAYING:
		level_animation.play(level)
		enemy_animation.play(level)


## Called when the level starts or the player respawns.
func _player_spawned():
	_log.debug("Player has respawned. Resume any paused animations.")
	if Global.replay_mode != InputRecorder.RecorderState.PLAYING:
		level_animation.play()
		enemy_animation.play()


## Called every time the player dies.
func _player_killed():
	_log.debug("Player died. Level progression is paused until the player respawns.")
	level_animation.pause()
	enemy_animation.pause()


## Called immediately when the player dies. Play elements should remain active and visible.
func _game_over():
	_log.debug("Game has ended, stop spawning things immediately.")
	level_animation.play("RESET")
	enemy_animation.play("RESET")


## Set the level animation to use. Should be the same for both animation players.
func _set_level(index):
	level = levels[index]
