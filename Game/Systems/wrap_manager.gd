class_name WrapManager extends Node2D
## Handles spawn positions and screen wrapping for all objects in the game.
##
## This class maintains three reference rectangles.
##   1. [member player_wrap] is used to wrap the player on the edges of the screen.
##   2. [member object_wrap] is the same size as [member player_wrap], but moves to stay
## near the edge of the screen so objects don't disappear.
##   3. [member object_spawn] will follow the player and is used for putting new things
## right next to the player instead of spawing on the other side of the map.
## [br][br]
## Rectangle sizes may be updated by animations during the level, so don't cache values.[br]
## Finally, [method random_spawn_pos] and [method random_edge_pos] can be used to get a random point
## near the player, or on the edge of the map. Use this when spawning objects or bosses.

## The screen resolution in the editor. The actual resolution is set through canvas scaling,
## so this number is still valid on any screen size. 
const screen_size = Vector2(1920, 1080)

## The size of the actual play space in terms of screen sizes. (2,2) is 3840X2160 pixels.
@export var play_size := Vector2(2, 2)

## The buffer size to add to the object wrap zone. Calculated in the same way as [member play_size].
## Should be less than (1, 1) and less than half of [member play_size] to avoid weirdness.
@export var wrap_margin := Vector2(0.3, 0.3)

## How large should the spawn area be when compared to the screen? 1.0 means objects spawn at the
## camera edge. Make this bigger if large objects become visible when they shouldn't.
@export var spawn_margin := Vector2(1.1, 1.1)

## The wrapping zone for objects. Changes its position based on the player position.
@onready var object_wrap := $ObjectWrap
## The wrapping zone for the player. Anchored to the world.
@onready var player_wrap := $PlayerWrap
## The spawn edge for objects. Follows the player and spawns just outside of view.
@onready var object_spawn = $ObjectSpawn


func _ready():
	Global.screen_wrap = self


func _physics_process(_delta):
	_update_wrap_zone()


## Returns a valid spawn position near the player, but outside of the player's view.
func random_spawn_pos() -> Vector2:
	return _random_edge(object_spawn)

## Returns a valid spawn position on the edge of the map. Spawn position is far from the player's view.
func random_edge_pos() -> Vector2:
	return _random_edge(object_wrap)


# Returns a position along one of the edges of this rectangle.
func _random_edge(wrap_window) -> Vector2:
	var width = wrap_window.width
	var height = wrap_window.height
	var total_perimeter = 2 * (width + height)
	var rand_point = randf_range(0, total_perimeter)

	# Adjust rand_point and return immediately when the correct edge is found.
	if rand_point < width:
		# Top edge
		return Vector2(wrap_window.left + rand_point, wrap_window.top)
	
	rand_point -= width
	if rand_point < height:
		# Right edge
		return Vector2(wrap_window.right, wrap_window.top + rand_point)
	
	rand_point -= height
	if rand_point < width:
		# Bottom edge
		return Vector2(wrap_window.right - rand_point, wrap_window.bottom)
	
	# Left edge, remaining case
	rand_point -= width
	return Vector2(wrap_window.left, wrap_window.top + rand_point)


# Update the sizes and positions of all the wrap rects.
func _update_wrap_zone():
	player_wrap.size = play_size * screen_size
	player_wrap.position = play_size * screen_size / -2.0
	
	object_spawn.size = screen_size * spawn_margin
	object_spawn.position = Global.player_pos - (screen_size * spawn_margin / 2.0)
	
	var offset_dir = Vector2(-1 if Global.player_pos.x < 0 else 1, -1 if Global.player_pos.y < 0 else 1)
	object_wrap.size = play_size * screen_size
	object_wrap.position = play_size * screen_size / -2.0 + screen_size * wrap_margin * offset_dir
