class_name AutoloadGlobal extends Node
## Global singleton. Stores all public values.
##
## I'm trying to not put so much stuff into these global singletons, so I'm going to try
## stubbing things out and linking to game elements instead. So, the player location will be
## the location of the player object. I can ask these object to self-identify and self-register,
## and will return default values if they don't exist (if I'm not in the game scene).
## @tutorial(Writing Godot Documentation): https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_documentation_comments.html

## Used by the debugging tools to quickly change the game settings.
const ALT_SAVE_FILES = [&"save_data_1", &"save_data_2", &"save_data_3"]
## Name of the save location for game settings.
const SETTINGS = &"Settings"

## The current state of the game.
enum GameState {
	## The game is being loaded, or there is no Game scene (A.K.A. I'm running a scene that isn't Game.tscn)
	BOOT,
	## We are sitting in the main menu or one of its submenus (settings, etc.)
	MAIN_MENU,
	## The game is active.
	PLAYING,
	## The game is paused while the player is selecting an upgrade.
	UPGRADING,
	## The pause menu is open.
	PAUSED,
	## The game has ended, but we aren't at the main menu yet.
	GAME_OVER,
}

## Unique identifiers for each asteroid (non-enemy unit) in the game.
enum AsteroidTypes{
	ASTEROID_0, # Smallest
	ASTEROID_1,
	ASTEROID_2,
	ASTEROID_3,
	ASTEROID_4,
	ASTEROID_5,
	ASTEROID_6,
	ASTEROID_7,
	ASTEROID_8,
	ASTEROID_9, # Largest
}

## Unique identifiers for each enemy or boss in the game.
enum EnemyUnitTypes {
	ENEMY_0
}

## The current state of the game. This is a pass-through to the Game script, but it will return
## [param BOOT] as the default value if that node is not connected.
var game_state: GameState:
	get:
		if is_instance_valid(_game):
			return _game.game_state
		else:
			Print.from("GameState", "No valid game scene detected. Game state is BOOT.", Print.VERBOSE)
			return GameState.BOOT

## A reference to the screen wrap manager. This object manages the spawn zone and play area borders.
var screen_wrap: WrapManager

## The global position of the player. If the player is not defined, returns Vector2.ZERO instead.
var player_pos: Vector2:
	get:
		if player != null:
			return player.global_position
		return Vector2.ZERO

## A reference to the Player. Player will set this when it loads.
var player # TODO: Add static typing when we get rid of the old player. : Player

## Set by the [InputRecorder] when it is playing back a recording. Events will be recorded or played
## back when set to RECORDING or PLAYING.
var replay_mode := InputRecorder.RecorderState.DISABLED

## Simple readonly UUID. This will return a unique ID each time, and will never repeat while the
## program is running.
var uuid: int:
	get:
		_uuid += 1
		return _uuid
## Returns the last generated uuid. Does not have a side effect.
var last_uuid: int:
	get:
		return _uuid
var _uuid := -1

## A reference to the Game script. Game will set this when it loads.
var _game

# Logger for general game events, like saving and loading.
var _game_log: Logger

# All of the save data for the game. (Each round is not saved, so we just use this for everything.)
# This is the lazy approach, but if it works, it works.
var _save_data := ConfigFile.new()

# Where to save:
var _save_location := &"user://save_data.srs"
var _save_backup_location := &"user://fallback_save.srs"
## Enabled by debugging scripts if you want to change settings without commiting the changes to the
## game save file on disk. Will disable ALL progress saving.
var skip_save := false

## Dependencies:
## [br][PrintDefaults] for initializing loggers before they are needed.
func _ready():
	# Silence registering all the things.
	Print.get_logger("Print").print_level = Logger.LogLevel.WARNING
	# Set all of the loggers in the project to default values (save all, output warnings only)
	for logger in PrintDefaults.print_loggers:
		Print.create_logger(logger, PrintDefaults.default_console_level, PrintDefaults.default_archive_level)
	# Get our logger.
	_game_log = Print.get_logger("Game")
	_load_from_file()


## Update the file location where we get the save data. Useful for movie maker mode, etc.
## This will attempt to open the save file immediately. If it fails, the current settings
## will be used instead.
func change_save_file_location(file_name):
	_save_location = "user://" + file_name + ".srs"
	_save_backup_location = "user://" + file_name + "_bak.srs"
	_load_from_file()


## Saves a value to [code]user://save_data.srs[/code]. Saving happens immediately and is automatic.
## Data is put under the default "Game" section in the save file.
func save_data(key: StringName, value):
	save_other("Game", key, value)


## Saves a value to [code]user://save_data.srs[/code]. Saving happens immediately and is automatic.
## [param section] is used to group data. If you just need to save one quick value, use
## [method save_data] instead.
## Use [param skip_write] to skip writing to the disk. Only use this if you are writing multiple
## keys at one time. Otherwise, leave this false to ensure that all data makes it to the disk.
func save_other(section: StringName, key: StringName, value, skip_write := false):
	_save_data.set_value(section, key, value)
	# Do this immediately. If we have skip_save or skip_write set, we will NOT write to the file.
	if skip_write:
		pass
	elif skip_save:
		_game_log.info("Game data was updated, but nothing was written to disk. (skip_save is true)")
	else:
		_save_to_file()


## Loads a value from [code]user://save_data.srs[/code]. If the value for [param key] is not found,
## will return null. Loads from the default "Game" section of the save file.
func load_data(key: StringName):
	return load_other("Game", key)


## Loads a value from [param section] in [code]user://save_data.srs[/code]. Make sure to match
## [param section] and [param key] from the save function. Returns null if the key doesn't exist.
func load_other(section: StringName, key: StringName):
	if _save_data.has_section_key(section, key):
		return _save_data.get_value(section, key)
	return null


## Shorthand for [code]Console.add_command().set_description().register()[/code]
## [param target] is most likely self.
func register_command(command_name, target, function_name, description = ""):
	Console.add_command(command_name, target, function_name).set_description(description).register()


# Save _save_data to _save_location and _save_backup_location.
func _save_to_file():
	_game_log.debug("Saving game to " + _save_location)
	var err = _save_data.save_encrypted_pass(_save_location, "password") # Data security is my passion.
	if err:
		_game_log.error("Save game data to %s failed with error %s!" % [_save_location, error_string(err)])
	else:
		_game_log.debug("Also saving game to " + _save_backup_location + " as a failsafe.")
		err = _save_data.save_encrypted_pass(_save_backup_location, "strongerPassword!") # Hackers beware.
	if err:
		_game_log.error("Save game data to %s failed with error %s!" % [_save_backup_location, error_string(err)])
	else:
		_game_log.info("Game Data Saved")


# Loads _save_data from _save_location. If that fails, loads from _save_backup_location.
func _load_from_file():
	_game_log.debug("Loading game from " + _save_location)
	var err1 = _save_data.load_encrypted_pass(_save_location, "password") # They'll never guess this one!
	if err1:
		var err2 = _save_data.load_encrypted_pass(_save_backup_location, "strongerPassword!") # 12345
		if err2 == ERR_FILE_NOT_FOUND:
			_game_log.info("Save file not found! This is not an issue if we are starting the game for the first time.")
		elif err2:
			_game_log.warning("Loading save data failed! This is an issue if the game was saved previously. Error: " + error_string(err1))
		else:
			_game_log.warning("Save data file %s is corrupted! Thankfully, we saved a second copy to %s. Error: %s" %\
					[_save_location, _save_backup_location, error_string(err1)])
			get_tree().call_group("SaveLoad", "_load_data")
	else:
		_game_log.info("Successfully loaded save game data.")
		get_tree().call_group("SaveLoad", "_load_data")
