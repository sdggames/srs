# GdUnit generated TestSuite
class_name InputRecorderTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')

# TestSuite generated from
const __source = 'res://Game/DebuggingTools/InputRecorder/input_recorder.gd'
const GAME = preload("res://Game/game.tscn")

# Simple test to make sure that I didn't leave the Input Recorder active when checking in.
func test_scene_state() -> void:
	var scene = auto_free(GAME.instantiate())
	assert_object(scene).is_not_null()
	
	var input_recorder = scene.get_node("DevTools/InputRecorder")
	assert_object(input_recorder).is_not_null()
	
	var state = input_recorder.get("state")
	assert_int(state).is_equal(InputRecorder.RecorderState.IDLE)
