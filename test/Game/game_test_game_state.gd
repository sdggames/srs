# GdUnit generated TestSuite
class_name GameTest_GameState
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')

# TestSuite generated from
const __source = 'res://Game/game.gd'
const states = [
		[Global.GameState.BOOT],
		[Global.GameState.MAIN_MENU],
		[Global.GameState.PLAYING],
		[Global.GameState.UPGRADING],
		[Global.GameState.PAUSED],
		[Global.GameState.GAME_OVER]
	]


## The game node.
var game: Game
## The transition manager (animation player mock).
var transitions
## Scene runner holding game scene.
var runner
## Game Events
var group_signals

func before_test():
	game = auto_free(load("res://test/Game/game_test.tscn").instantiate())
	runner = scene_runner(game)
	transitions = game.transition_manager
	group_signals = monitor_signals(game.find_child("GameEvents"))


## Make sure we don't leave a mess.
func after_test():
	get_tree().paused = false


## Make sure that we are passing the game states to Global as expected.
@warning_ignore("unused_parameter")
func test_global_game_state(state, test_parameters = states) -> void:
	game._state = state
	assert_int(Global.game_state).is_equal(game.game_state)


## Make sure that we are booting into the Main Menu properly.
## We need to load the main menu, then change the game state.
## State transition happens after the animation completes.
func test_gameState_boot() -> void:
	game = auto_free(load("res://test/Game/game_test.tscn").instantiate())
	
	assert_int(game.game_state).is_equal(Global.GameState.BOOT)
	add_child(game)
	transitions = game.transition_manager
	
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.BOOT)
	# We are passing this up, check that it is working.
	assert_int(Global.game_state).is_equal(game.game_state)
	
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)


## Make sure that we are transitioning from the menu to the play state.
## Transition occurs after the animation is complete.
func test_gameState_play() -> void:
	# Enter menu
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	
	# Test
	game._on_main_menu_start_pressed()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Make sure that we are transitioning from the menu to the play state.
## Transition occurs after the animation is complete.
func test_gameState_play_immediate() -> void:
	# Enter menu
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.BOOT)
	
	# Test
	game._on_main_menu_start_pressed()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)

## Make sure that we are transitioning from the menu to the play state.
## Transition occurs after the animation is complete.
func test_gameState_multiple_start() -> void:
	# Enter menu
	transitions.test_finish_animation()
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	assert_int(transitions.animation_count).is_equal(0)
	
	# Test
	game._on_main_menu_start_pressed()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	game._on_main_menu_start_pressed()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Make sure that we are transitioning from the play state to the upgrades menu and back.
## We should change states immediately when opening the menu, but after the menu close animation.
func test_gameState_upgrading() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Play.
	game._on_main_menu_start_pressed()
	assert_int(transitions.animation_count).is_equal(1)
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)

	# Test
	for i in range(3):
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
		game._on_level_up()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
		transitions.test_finish_animation()
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
		game._on_upgrades_menu_upgrade_selected()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
		transitions.test_finish_animation()
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Make sure that we are transitioning from the play state to the upgrades menu and back.
## This should still work if we click the button before the transition is complete.
func test_gameState_upgrading_immediate() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Play.
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)

	# Test
	for i in range(3):
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
		game._on_level_up()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
		game._on_upgrades_menu_upgrade_selected()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
		transitions.test_finish_animation()
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Make sure that we are transitioning from the play state to game over and back.
func test_gameState_gameover() -> void:
	# Enter menu
	transitions.test_finish_animation()
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	
	# Test
	for i in range(3):
		# Start game
		game._on_main_menu_start_pressed()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
		transitions.test_finish_animation()
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
		# End game.
		game._on_player_died()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.GAME_OVER)
		game._on_level_up()
		assert_int(transitions.animation_count).is_equal(1)
		assert_int(game.game_state).is_equal(Global.GameState.GAME_OVER)
		transitions.test_finish_animation()
		assert_int(transitions.animation_count).is_equal(0)
		assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)


## Make sure that we can't open the upgrades menu after we died.
func test_gameState_upgrade_after_gameover() -> void:
	# Enter menu
	transitions.test_finish_animation()
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	
	# Test
	# Start game
	game._on_main_menu_start_pressed()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	# End game.
	game._on_player_died()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.GAME_OVER)
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)

## Make sure that we can only pause from the play state.
## Transition occurs immeditately.
func test_gameState_pause() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Start game
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	
	# Test
	# Game is not paused.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	
	# Pause - should be immediate.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)


## Pause the game, then immediately un-pause before the pause animation completes.
func test_gameState_pause_unpause_immediate() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Start game
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	
	# Test
	# Game is not paused.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	
	# Pause - should be immediate.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Un-pause.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Finish un-pause.
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Pause the game, wait for the pause menu, then unpause.
func test_gameState_pause_unpause_delayed() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Start game
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	
	# Test
	# Game is not paused.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	
	# Pause - should be immediate.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Wait for menu to come up - no state change.
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Un-pause - starts animation.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Finish un-pause.
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Use the button to un-pause the game. Hit the button after the menu is up.
func test_gameState_unpause_button() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Start game
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	
	# Test
	# Game is not paused.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	
	# Pause - should be immediate.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Wait for menu to come up - no state change.
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Un-pause - starts animation.
	game._on_pause_menu_resume()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Finish un-pause.
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Use the button to un-pause the game. Hit the button before the menu has fully activated.
func test_gameState_unpause_button_immediate() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Start game
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	
	# Test
	# Game is not paused.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	
	# Pause - should be immediate.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Un-pause - starts animation.
	game._on_pause_menu_resume()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.PAUSED)
	
	# Finish un-pause.
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Make sure that we can't pause from any state that isn't the play state.
## Test 1 - pausing during the main menu and transition to play state.
func test_gameState_invalid_pause_main_menu() -> void:
	# Enter menu
	assert_int(game.game_state).is_equal(Global.GameState.BOOT)
	assert_bool(get_tree().paused).is_false()
	
	# Pause during start.
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_false()
	
	# Finish entering menu.
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	assert_bool(get_tree().paused).is_false()
	
	# Pause from menu - should do nothing.
	runner.simulate_key_pressed(KEY_ESCAPE)
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	
	# Start game
	game._on_main_menu_start_pressed()
	# Pause while starting - should do nothing.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Make sure that we can't pause from any state that isn't the play state.
## Test 2 - pausing when transitioning from play state to main menu.
func test_gameState_invalid_pause_game_over() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Start game
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	
	# Player died - should be transitioning to menu.
	game._on_player_died()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.GAME_OVER)
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.GAME_OVER)
	transitions.test_finish_animation()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.MAIN_MENU)


## Make sure that we can't pause from any state that isn't the play state.
## Test 3 - pausing when transitioning from play state to upgrades menu and back.
func test_gameState_invalid_pause_upgrading() -> void:
	# Enter menu
	transitions.test_finish_animation()
	
	# Play.
	game._on_main_menu_start_pressed()
	transitions.test_finish_animation()

	# Test
	# Open Upgrades Menu.
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)
	game._on_level_up()
	
	# Try to pause.
	assert_bool(get_tree().paused).is_true() # True because the upgrade menu stops the game.
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.UPGRADING) # We should not be paused.
	
	# Finish animation.
	transitions.test_finish_animation()

	# Try to pause.
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_true()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
	
	# Close upgrades menu.
	game._on_upgrades_menu_upgrade_selected()
	
	# Try to pause.
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
	runner.simulate_key_pressed(KEY_ESCAPE)
	await await_idle_frame()
	assert_bool(get_tree().paused).is_false()
	assert_int(transitions.animation_count).is_equal(1)
	assert_int(game.game_state).is_equal(Global.GameState.UPGRADING)
 	
	# Finish closing upgrades menu.
	transitions.test_finish_animation()
	assert_int(transitions.animation_count).is_equal(0)
	assert_int(game.game_state).is_equal(Global.GameState.PLAYING)


## Test starting the game for the first time.
func test_gameEvents_game_start() -> void:
	transitions.test_finish_animation()
	game._on_main_menu_start_pressed()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_start"])


## Test setting the game state to Global.GameState.PLAYING.
func test_gameEvents_player_spawned() -> void:
	game.game_state = Global.GameState.PLAYING
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_player_spawned"])
	
	# Double-setting should do nothing.
	game.game_state = Global.GameState.PLAYING
	await assert_signal(group_signals).wait_until(50).is_not_emitted("group_method_called", ["_player_spawned"])


## Test calling _on_player_died() - This is the only game state signal that isn't directly connected to a GameState,
## at least for the time being. If that changes, I'll have to update this test to match.
func test_gameEvents_player_killed() -> void:
	game._on_player_died()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_player_killed"])


## Test setting the game state to Global.GameState.GAME_OVER.
func test_gameEvents_game_over() -> void:
	game.game_state = Global.GameState.GAME_OVER
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_over"])
	
	# Double-setting should do nothing.
	game.game_state = Global.GameState.GAME_OVER
	await assert_signal(group_signals).wait_until(50).is_not_emitted("group_method_called", ["_game_over"])


## Test setting the game state to Global.GameState.MAIN_MENU.
func test_gameEvents_game_reset() -> void:
	game.game_state = Global.GameState.GAME_OVER
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_over"])
	game.game_state = Global.GameState.MAIN_MENU
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_reset"])
	
	# Double-setting should do nothing.
	game.game_state = Global.GameState.MAIN_MENU
	await assert_signal(group_signals).wait_until(50).is_not_emitted("group_method_called", ["_game_reset"])


## Integration test to verify all game events are sent during real game state transitions.
func test_gameState_triggers_gameEvents():
	# Make sure nothing was sent already.
	await assert_signal(group_signals).wait_until(50).is_not_emitted("group_method_called")
	
	# Enter menu
	transitions.test_finish_animation()
	await assert_signal(group_signals).wait_until(50).is_not_emitted("group_method_called")
	
	# Start game.
	game._on_main_menu_start_pressed()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_start"])
	transitions.test_finish_animation()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_player_spawned"])
	
	# Player died.
	game._on_player_died()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_player_killed"])
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_over"])
	
	# Back to main menu.
	transitions.test_finish_animation()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_game_reset"])
	
	# Start game.
	game._on_main_menu_start_pressed()
	await assert_signal(group_signals).wait_until(50).is_not_emitted("group_method_called", ["_player_spawned"])
	transitions.test_finish_animation()
	await assert_signal(group_signals).wait_until(150).is_emitted("group_method_called", ["_player_spawned"])
