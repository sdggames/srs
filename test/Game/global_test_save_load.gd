# GdUnit generated TestSuite
class_name GlobalsextendsNodeTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')

# TestSuite generated from
const __source = 'res://Game/global.gd'


# Replace these so we don't nuke the save files.
func before():
	Global._save_location = &"user://test_data.srs"
	Global._save_backup_location = &"user://test_data_bak.srs"
	Print.start_all()
	Print.get_logger("Game").print_level = Logger.LogLevel.VERBOSE
	Print.get_logger("Global").print_level = Logger.LogLevel.VERBOSE


func test_save_data() -> void:
	Global.save_data("test_value", "secret_number")
	assert_that(Global._save_data.get_value("Game", "test_value")).is_equal("secret_number")


func test_load_data() -> void:
	Global._save_data.set_value("Game", "test_value", "other_secret_number")
	assert_that(Global.load_data("test_value")).is_equal("other_secret_number")


func test_load_data_from_file() -> void:
	Global.save_data("test_value", "secret_number")
	# Delete all of this stuff. Let's hope it was saved to disk!
	Global._save_data = ConfigFile.new()
	# Pull from disk.
	Global._load_from_file()
	assert_str(Global.load_data("test_value")).is_equal("secret_number")


func test_save_load_destructive() -> void:
	Global.save_data("test_value", "other_secret_phrase")
	# OOPS, hope we have a backup!
	DirAccess.remove_absolute(Global._save_location)
	Global._save_data = ConfigFile.new()
	# It's OK, this will fall back to the second file! We did write a second file, right?
	Global._load_from_file()
	assert_str(Global.load_data("test_value")).is_equal("other_secret_phrase")


func test_save_other() -> void:
	Global.save_other("Testing", "test_value", "secret_number")
	assert_that(Global._save_data.get_value("Testing", "test_value")).is_equal("secret_number")


func test_load_other() -> void:
	Global._save_data.set_value("Testing", "test_value", "other_secret_number")
	assert_that(Global.load_other("Testing", "test_value")).is_equal("other_secret_number")


func test_load_other_from_file() -> void:
	Global.save_other("Testing", "test_value", "secret_number")
	# Delete all of this stuff. Let's hope it was saved to disk!
	Global._save_data = ConfigFile.new()
	# Pull from disk.
	Global._load_from_file()
	assert_str(Global.load_other("Testing", "test_value")).is_equal("secret_number")


func test_save_load_other_destructive() -> void:
	Global.save_other("Testing", "test_value", "other_secret_phrase")
	# OOPS, hope we have a backup!
	DirAccess.remove_absolute(Global._save_location)
	Global._save_data = ConfigFile.new()
	# It's OK, this will fall back to the second file! We did write a second file, right?
	Global._load_from_file()
	assert_str(Global.load_other("Testing", "test_value")).is_equal("other_secret_phrase")
