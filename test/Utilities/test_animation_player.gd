class_name Test_Animation_Player extends Node

signal animation_finished(anim: String)

## A list of all pending animations.
var next_anims: Array[String] = []

## The number of pending animations.
var animation_count: int:
	get:
		return next_anims.size()


## Clear all pending animations and set this one instead. 
func play(anim: String):
	next_anims = [anim]


## Return true if we have one or more active animations.
func is_playing():
	return animation_count > 0


## Add this animation to the list.
func queue(anim: String):
	next_anims.append(anim)


## Unit testing - complete the animation and emit [signal animaiton_finished].
func test_finish_animation():
	if next_anims.size() > 0:
		animation_finished.emit(next_anims.pop_front())
