# Space Rock Survivors

The sovereign nation of Arcadia has sworn to protect its natural beauty from all alien invaders.
What beauty? It's just a bunch of space rocks.
Your mission: Harvest, extract, survive.

Play through a colorful love-letter to arcade vector graphics with a new Roguelike.

This game was made in Godot 4. It has CI-CD deployment, a built in console, and logging functionality.

### IMPORTANT! - This project uses submodules. Use the following commands to run the code.
1. `git clone <project.git>` - Replace project.git with the URL of this repository.
2. `cd project` - Enter the project folder.
3. `git submodule update --init` - Downloads the code for each submodule into the assets folder.
